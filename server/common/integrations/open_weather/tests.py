from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase

from common.integrations.open_weather.exception import ValidationError, UnauthorizedError, NotFound
from common.integrations.open_weather.client import OpenWeatherAPIClient

User = get_user_model()


class OpenWeatherAPIClientTest(TestCase):

    def test_search_weather_ok(self):
        open_weather = OpenWeatherAPIClient()
        response = open_weather.search_weather({"q": "Fortaleza,br"})

        self.assertIn("base", response)
        self.assertIn("name", response)
        self.assertIn("weather", response)
        self.assertIn("coord", response)

    def test_search_weather_ValidationError(self):
        with self.assertRaises(ValidationError):
            open_weather = OpenWeatherAPIClient()
            open_weather.search_weather({"appid": settings.OPEN_WEATHER_API_APP_ID})

    def test_search_weather_UnauthorizedError(self):
        with self.assertRaises(UnauthorizedError):
            open_weather = OpenWeatherAPIClient()
            open_weather.search_weather({})

    def test_search_weather_NotFound(self):
        with self.assertRaises(NotFound):
            open_weather = OpenWeatherAPIClient()
            open_weather.search_weather({"q": "aaaa,br"})
