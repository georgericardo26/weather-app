from django.test import TestCase

from core.models import HistoryWeatherModel


class HistoryWeatherModelTest(TestCase):

    def test_create_user_successful(self):
        user_model = HistoryWeatherModel.objects.create(**{
            "name_city": "NEW YORK",
            "temp": 30.1
        })

        self.assertEqual(user_model.name_city, "NEW YORK")
        self.assertEqual(user_model.temp, 30.1)
