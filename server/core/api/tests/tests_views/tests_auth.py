from unittest import mock

from django.contrib.auth import get_user_model
from rest_framework import status
from django.test import TestCase, RequestFactory
from django.urls import reverse

from core.api.tests.tests_views.utils import GetTokenTest
from core.api.views.auth_views import AuthView


User = get_user_model()


class UserAuthTest(TestCase):
    url_name = "v1:create_user"

    USER = {
        "username": "testuser",
        "password": "pass2000@",
        "email": "test@test.com"
    }

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(**self.USER)
        self.token_client = GetTokenTest()

        self.data = {
            **self.token_client.data,
            "username": self.USER["username"],
            "password": self.USER["password"]
        }

    def get_authentication_object(self, username, password):
        return self.token_client.get_token(username, password)

    @mock.patch('core.api.views.auth_views.AuthView.return_oauth2_token')
    def test_auth_user_successfully(self, mock_AuthView):
        auth_obj = self.get_authentication_object(self.USER["username"], self.USER["password"])
        mock_AuthView.return_value = auth_obj
        request = self.factory.post(reverse(self.url_name), data=self.data)

        response = AuthView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("access_token", response.data)
        self.assertIn("expires_in", response.data)
        self.assertIn("token_type", response.data)
        self.assertIn("scope", response.data)
        self.assertIn("refresh_token", response.data)
        self.assertIn("user", response.data)

    def test_auth_user_no_body_data(self):
        message_fail = "This field is required."
        request = self.factory.post(reverse(self.url_name), data={})
        response = AuthView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("client_id", response.data)
        self.assertIn(message_fail, response.data["client_id"])
        self.assertIn("client_secret", response.data)
        self.assertIn(message_fail, response.data["client_secret"])
        self.assertIn("grant_type", response.data)
        self.assertIn(message_fail, response.data["grant_type"])
        self.assertIn("username", response.data)
        self.assertIn(message_fail, response.data["username"])
        self.assertIn("password", response.data)
        self.assertIn(message_fail, response.data["password"])

    def test_auth_user_using_invalid_user(self):
        self.get_authentication_object(self.USER["username"], self.USER["password"])
        self.data["username"] = "aabbcc"

        request = self.factory.post(reverse(self.url_name), data=self.data)
        response = AuthView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("error: User not found", response.data)

    def test_auth_user_using_invalid_password(self):
        self.get_authentication_object(self.USER["username"], self.USER["password"])
        self.data["password"] = "aabbcc"

        request = self.factory.post(reverse(self.url_name), data=self.data)
        response = AuthView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertIn("detail", response.data)
        self.assertEqual(response.data["detail"], "You do not have permission to perform this action.")

    @mock.patch('core.api.views.auth_views.AuthView.return_oauth2_token')
    def test_auth_user_using_invalid_client_id(self, mock_AuthView):
        self.token_client.data["client_id"] = "000000000000"
        auth_obj = self.get_authentication_object(self.USER["username"], self.USER["password"])
        mock_AuthView.return_value = auth_obj

        request = self.factory.post(reverse(self.url_name), data=self.data)
        response = AuthView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIn("error", response.data)
        self.assertEqual(response.data["error"], "invalid_client")
