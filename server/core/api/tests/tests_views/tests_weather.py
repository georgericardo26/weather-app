from django.contrib.auth import get_user_model
from rest_framework import status
from django.test import TestCase, RequestFactory
from django.urls import reverse
from core.api.tests.tests_views.utils import GetTokenTest
from core.api.views.weather_views import WeatherView, HistoryWeatherView

User = get_user_model()


class WeatherBaseTest(TestCase):

    USER = {
        "username": "testuser",
        "password": "pass2000@",
        "email": "test@test.com"
    }

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(**self.USER)

    def get_token(self, username, password):
        token = GetTokenTest()
        return token.get_token(username, password).json()


class WeatherSearchTest(WeatherBaseTest):
    url_name = "v1:weather_search"

    def test_search_weather_successful(self):
        token = self.get_token(self.USER["username"], self.USER["password"])
        request = self.factory.get(reverse(self.url_name),
                                   {"zip": "94040,us"},
                                   Authorization=f"Bearer {token['access_token']}")

        request.user = self.user
        response = WeatherView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("main", response.data)
        self.assertIn("name", response.data)

    def test_search_weather_without_parameters(self):
        token = self.get_token(self.USER["username"], self.USER["password"])
        request = self.factory.get(reverse(self.url_name),
                                   {},
                                   Authorization=f"Bearer {token['access_token']}")

        request.user = self.user
        response = WeatherView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertIn("code", response.data)
        self.assertIn("message", response.data)
        self.assertEqual(response.data["code"], "missing_query_parameters")
        self.assertEqual(response.data["message"], "One or more query parameter could not be validated.")

    def test_search_weather_without_authorization_header(self):
        request = self.factory.get(reverse(self.url_name),
                                   {"zip": "94040,us"})

        request.user = self.user
        response = WeatherView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIn("detail", response.data)
        self.assertEqual(response.data["detail"], "Authentication credentials were not provided.")

    def test_integration_search_weather(self):
        token = self.get_token(self.USER["username"], self.USER["password"])
        response = self.client.get(reverse(self.url_name),
                                   {"zip": "94040,us"},
                                   Authorization=f"Bearer {token['access_token']}")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("main", response.data)
        self.assertIn("name", response.data)


class WeatherHistoryTest(WeatherBaseTest):
    url_name = "v1:weather_history"

    def test_history_search_successful(self):
        token = self.get_token(self.USER["username"], self.USER["password"])

        # Search Weather firstly
        self.client.get(reverse("v1:weather_search"),
                        {"zip": "94040,us"},
                        Authorization=f"Bearer {token['access_token']}")

        # Get History
        request = self.factory.get(reverse(self.url_name),
                                   Authorization=f"Bearer {token['access_token']}")

        request.user = self.user
        response_search_history = HistoryWeatherView.as_view()(request)

        self.assertEqual(response_search_history.status_code, status.HTTP_200_OK)
        self.assertIn("results", response_search_history.data)
        self.assertEqual(len(response_search_history.data["results"]), 1)

    def test_history_search_without_authorization_header(self):

        # Get History
        request = self.factory.get(reverse(self.url_name))

        request.user = self.user
        response_search_history = HistoryWeatherView.as_view()(request)

        self.assertEqual(response_search_history.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_integration_history_search(self):
        token = self.get_token(self.USER["username"], self.USER["password"])
        response = self.client.get(reverse(self.url_name),
                                   Authorization=f"Bearer {token['access_token']}")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
