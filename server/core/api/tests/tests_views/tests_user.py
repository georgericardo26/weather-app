from django.contrib.auth import get_user_model
from rest_framework import status
from django.test import TestCase, RequestFactory
from django.urls import reverse
from core.api.views.user_views import UserCreateView

User = get_user_model()


class UserCreateTest(TestCase):
    url_name = "v1:create_user"

    USER = {
        "username": "testuser",
        "password": "pass2000@",
        "email": "test@test.com"
    }

    def setUp(self):
        self.factory = RequestFactory()

    def test_create_user_successful(self):
        request = self.factory.post(reverse(self.url_name), data=self.USER)

        response = UserCreateView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("username", response.data)
        self.assertIn("email", response.data)

    def test_create_user_already_exists(self):
        User.objects.create_user(**self.USER)
        request = self.factory.post(reverse(self.url_name), data=self.USER)

        response = UserCreateView.as_view()(request)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("A user with that username already exists.", response.data["username"])

    def test_integration_create_user(self):
        response = self.client.post(reverse(self.url_name), data=self.USER)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("username", response.data)
        self.assertIn("email", response.data)
