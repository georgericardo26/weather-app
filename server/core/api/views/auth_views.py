import json
import logging
import requests

from django.contrib.auth import get_user_model, authenticate
from django.http import Http404
from django.urls import reverse
from rest_framework import generics, permissions, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response


from core.api.serializers.auth_serializers import AuthSerializer, AuthOutputSerializer, AuthInputSerializer

User = get_user_model()

logger = logging.getLogger(__name__)


class AuthView(generics.CreateAPIView):
    """
    View to authenticate user, throuth this view, we can access the oauth2
    endpoint to create and return a token.
    """
    queryset = User.objects.all()
    serializer_class = AuthSerializer
    input_serializer = AuthInputSerializer
    permission_classes = [permissions.AllowAny]
    pagination_class = None

    logger.info("Initiate user authentication...", key="UserAuth")

    def post(self, request, *args, **kwargs):

        input_serializer = self.input_serializer(data=request.data)
        input_serializer.is_valid(raise_exception=True)

        # validate user
        username = request.data.get("username")
        password = request.data.get("password")

        logger.info("Checking if user exists...", key="UserAuth")

        # check if user exists
        try:
            get_object_or_404(User, username=username)
        except Http404:
            logger.error("Authentication failed, User not found")
            return Response({"error: User not found"}, status.HTTP_400_BAD_REQUEST)

        # check username and password
        user = authenticate(username=username, password=password)

        if not user:
            logger.error("Authentication user failed!")
            raise PermissionDenied

        # Todo: This is not a best approach, I'll change after
        request_auth = self.return_oauth2_token(request)

        return self.return_response_user_auth(request_auth, user)

    def return_oauth2_token(self, request):
        return requests.post(
            request.build_absolute_uri(reverse('oauth2_provider:token')),
            data=json.dumps(request.data)
        )

    def return_response_user_auth(self, resp_auth, user):
        if resp_auth.status_code == status.HTTP_401_UNAUTHORIZED:
            return Response(resp_auth.json(), status=status.HTTP_401_UNAUTHORIZED)

        serializer_user = self.serializer_class(instance=user)

        obj = resp_auth.json()
        obj["user"] = serializer_user.data

        serializer = AuthOutputSerializer(
            data=obj
        )
        serializer.is_valid()

        logger.info("User authenticated successfully!", key="UserAuth")

        return Response(serializer.data, status=status.HTTP_201_CREATED)
