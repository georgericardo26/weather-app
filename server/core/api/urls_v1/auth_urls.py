from django.urls import path

from core.api.open_api_schema import DecoratedAuthView

urlpatterns = [
    path('', DecoratedAuthView, name='auth_user')
]
